package moscow.coding.robinhooditlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobinhooditlibApplication {

    public static void main(String[] args) {
        SpringApplication.run(RobinhooditlibApplication.class, args);
    }
}
